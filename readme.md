# Welcome to Weather

Weather is a simple iOS-app that shows you the current weather and a weekly forecast
based on your location.

In order to get started do the following:

- set up your environment for [React Native](https://facebook.github.io/react-native/docs/getting-started.html#content)
- run ```npm install```
- fill your [Dark Sky](https://darksky.net/dev/) secret key to ```config.js```
- hit run

#### Future improvements
- option to use either Celsius or Fahrenheit
- show forecast based on given location
- Android support