const colors = {
  main: '#e74c3c',
  accent: '#ecf0f1',
};

export default colors;
