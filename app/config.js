const KEY = ''; // add your secret key here

const config = {
  forecastApi: `https://api.darksky.net/forecast/${KEY}`,
};

export default config;
