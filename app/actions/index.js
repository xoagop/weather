import alt from '../alt';

class ForecastRelatedActions {
  constructor() {
    this.generateActions(
      'refresh',
      'refreshSucceeded',
      'refreshFailed',
    );
  }
}

const ForecastActions = alt.createActions(ForecastRelatedActions);

/* eslint-disable import/prefer-default-export */
export {
  ForecastActions,
};
/* eslint-enable import/prefer-default-export */
