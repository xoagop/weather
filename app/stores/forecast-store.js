import alt from '../alt';
import ForecastSource from '../sources/forecast-source';
import Weather from '../models/weather';
import {
  ForecastActions,
} from '../actions';

class ForecastStore {
  constructor() {
    this.bindAction(ForecastActions.refresh, this.onRefresh);
    this.bindAction(ForecastActions.refreshSucceeded, this.onRefreshSucceeded);
    this.bindAction(ForecastActions.refreshFailed, this.onRefreshFailed);

    this.registerAsync(ForecastSource);

    this.state = {
      weather: Weather.empty(),
      isRefreshing: true,
    };
  }

  onRefresh = (location) => {
    this.setState({
      isRefreshing: true,
    });
    this.getInstance().refresh(location)
      .catch(error => console.log('error occurred while refreshing forecast', error));
  };

  onRefreshSucceeded = (weather) => {
    this.setState({
      weather,
      isRefreshing: false,
    });
  };

  onRefreshFailed = () => {
    this.setState({
      weather: Weather.empty(),
      isRefreshing: false,
    });
  }
}

export default alt.createStore(ForecastStore, 'ForecastStore');
