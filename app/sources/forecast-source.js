import {
  ForecastActions,
} from '../actions';
import api from '../utils/api';
import Weather from '../models/weather';

const ForecastSource = {
  refresh: {
    remote(state, location) {
      return new Promise((resolve, reject) => {
        api.forecast(location)
          .then(response => response.json())
          .then(jsonObject => resolve(Weather.fromApiResponse(jsonObject)))
          .catch(error => reject(error));
      });
    },
    success: ForecastActions.refreshSucceeded,
    error: ForecastActions.refreshFailed,
  },
};

export default ForecastSource;
