import moment from 'moment';

class Forecast {
  constructor(
    icon,
    temperature,
    minTemperature,
    maxTemperature,
    precipProbability,
    time,
  ) {
    this.icon = icon;
    this.temperature = Math.floor(temperature);
    this.minTemperature = Math.floor(minTemperature);
    this.maxTemperature = Math.floor(maxTemperature);
    this.precipProbability = Math.floor(precipProbability);
    this.time = moment.unix(time);
  }

  get day() {
    return this.time.format('ddd');
  }

  static fromObject({
    icon,
    temperature,
    minTemperature,
    maxTemperature,
    precipProbability,
    time,
  }) {
    return new Forecast(
      icon,
      temperature,
      minTemperature,
      maxTemperature,
      precipProbability,
      time,
    );
  }

  static fromApiResponse(response) {
    if (!response) {
      return null;
    }

    return Forecast.fromObject({
      icon: response.icon,
      time: response.time,
      maxTemperature: response.temperatureMax,
      minTemperature: response.temperatureMin,
      precipProbability: response.precipProbability,
    });
  }

  static empty() {
    return new Forecast('', 0, 0, 0, 0);
  }
}

export default Forecast;
