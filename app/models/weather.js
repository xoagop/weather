import R from 'ramda';
import Forecast from './forecast';

class Weather {
  constructor(current, dailies) {
    this.current = current;
    this.dailies = dailies;
  }

  static fromObject({
    current,
    dailies,
  }) {
    return new Weather(current, dailies);
  }

  static fromApiResponse(response) {
    if (!response) {
      return null;
    }

    const { currently, daily: { data } } = response;
    const weekdays = R.drop(1, data);
    const today = data[0];

    return Weather.fromObject({
      current: Forecast.fromObject({
        icon: currently.icon,
        temperature: Math.floor(currently.temperature),
        precipProbability: Math.floor(currently.precipProbability),
        time: currently.time,
        minTemperature: Math.floor(today.temperatureMin),
        maxTemperature: Math.floor(today.temperatureMax),
      }),
      dailies: weekdays.map(Forecast.fromApiResponse),
    });
  }

  static empty() {
    return new Weather(Forecast.empty(), []);
  }
}

export default Weather;
