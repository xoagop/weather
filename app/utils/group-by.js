import R from 'ramda';

const groupBy = (key, items, transform = item => item) => {
  const group = (toObj, item) => {
    const itemKey = item[key];
    if (!itemKey) {
      return toObj;
    }

    let itemsToAdd = [transform(item)];
    if (toObj[itemKey]) {
      itemsToAdd = toObj[itemKey].concat([transform(item)]);
    }

    return R.assoc(itemKey, itemsToAdd, toObj);
  };

  return R.reduce(group, {}, items);
};

export default groupBy;
