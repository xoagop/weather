export const forecastIcons = {
  rain: 'rain',
  clear: 'clear-day',
  cloudy: 'cloudy',
  partlyCloudyNight: 'partly-cloudy-night',
  snow: 'snow',
};

const resolveIconFor = (forecastIcon) => {
  switch (forecastIcon) {
    case forecastIcons.rain:
      return 'ios-rainy';
    case forecastIcons.clear:
      return 'ios-sunny';
    case forecastIcons.cloudy:
      return 'ios-cloudy';
    case forecastIcons.partlyCloudyNight:
      return 'ios-cloudy-night';
    case forecastIcons.snow:
      return 'ios-snow';
    default:
      return 'ios-partly-sunny';
  }
};

export default resolveIconFor;
