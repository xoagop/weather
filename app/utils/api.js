import config from '../config';

const api = {
  forecast({ latitude, longitude }) {
    return fetch(`${config.forecastApi}/${latitude},${longitude}?units=si`);
  },
};

export default api;
