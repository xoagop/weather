import React from 'react';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';
import colors from '../../constants/colors';

const styles = StyleSheet.create({
  container: {
    marginTop: 5,
    flexDirection: 'row',
    alignItems: 'center',
  },
  header: {
    color: colors.accent,
    fontWeight: 'bold',
    fontSize: 20,
  },
});

const DailyHeader = ({ header }) => (
  <View style={styles.container}>
    <Text style={styles.header}>{header.toUpperCase()}</Text>
  </View>
);

DailyHeader.propTypes = {
  header: React.PropTypes.string.isRequired,
};

export default DailyHeader;
