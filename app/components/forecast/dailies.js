import React, { Component } from 'react';
import {
  ListView,
  RecyclerViewBackedScrollView,
  StyleSheet,
} from 'react-native';
import Daily from './daily';
import Forecast from '../../models/forecast';
import groupBy from '../../utils/group-by';
import DailyHeader from './daily-header';

const styles = StyleSheet.create({
  container: {
    paddingLeft: 35,
  },
});

class Dailies extends Component {
  static propTypes = {
    dailyForecast: React.PropTypes.arrayOf(React.PropTypes.instanceOf(Forecast)).isRequired,
  };

  constructor(props) {
    super(props);
    const dataSource = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2,
      sectionHeaderHasChanged: (s1, s2) => s1 !== s2,
    });
    this.state = {
      dataSource: dataSource.cloneWithRowsAndSections(groupBy('day', this.props.dailyForecast)),
    };
  }

  renderRow = (forecast) => (
    <Daily forecast={forecast} />
  );

  renderSectionHeader = (forecast, day) => (
    <DailyHeader header={day} />
  );

  render() {
    return (
      <ListView
        contentContainerStyle={styles.container}
        renderRow={this.renderRow}
        renderSectionHeader={this.renderSectionHeader}
        dataSource={this.state.dataSource}
        renderScrollComponent={props => <RecyclerViewBackedScrollView {...props} />}
        showsVerticalScrollIndicator={false}
      />
    );
  }
}

export default Dailies;
