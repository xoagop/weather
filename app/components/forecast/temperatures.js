import React from 'react';
import {
  View,
  StyleSheet,
  Text,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import colors from '../../constants/colors';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  temperature: {
    marginHorizontal: 5,
    color: colors.accent,
  },
});

const ICON_SIZE = 20;

const Temperatures = ({ minTemperature, maxTemperature }) => (
  <View style={styles.container}>
    <Icon
      name="ios-arrow-round-down"
      color={colors.accent}
      size={ICON_SIZE}
    />
    <Text style={styles.temperature}>{`${minTemperature}\u{00B0}`}</Text>
    <Icon
      name="ios-arrow-round-up"
      color={colors.accent}
      size={ICON_SIZE}
    />
    <Text style={styles.temperature}>{`${maxTemperature}\u{00B0}`}</Text>
  </View>
);

Temperatures.propTypes = {
  minTemperature: React.PropTypes.number.isRequired,
  maxTemperature: React.PropTypes.number.isRequired,
};

export default Temperatures;
