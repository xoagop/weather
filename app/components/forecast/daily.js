import React from 'react';
import {
  View,
  StyleSheet,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Forecast from '../../models/forecast';
import colors from '../../constants/colors';
import Temperatures from './temperatures';
import resolveIconFor from '../../utils/resolve-icon-for';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingRight: 20,
  },
  forecast: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    flex: 2,
  },
  icon: {
    paddingLeft: 10,
    alignSelf: 'center',
    width: 60,
    height: 60,
  },
});

const ICON_SIZE = 50;

const Daily = ({ forecast }) => (
  <View style={styles.container}>
    <View style={styles.forecast}>
      <Icon
        style={styles.icon}
        name={resolveIconFor(forecast.icon)}
        color={colors.accent}
        size={ICON_SIZE}
      />
      <Temperatures {...forecast} />
    </View>
  </View>
);

Daily.propTypes = {
  forecast: React.PropTypes.instanceOf(Forecast).isRequired,
};

export default Daily;
