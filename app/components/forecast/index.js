import React from 'react';
import {
  View,
  StyleSheet,
} from 'react-native';
import Forecast from '../../models/forecast';
import Dailies from './dailies';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

const ForecastView = ({ dailyForecast }) => (
  <View style={styles.container}>
    <Dailies dailyForecast={dailyForecast} />
  </View>
);

ForecastView.propTypes = {
  dailyForecast: React.PropTypes.arrayOf(React.PropTypes.instanceOf(Forecast)).isRequired,
};

export default ForecastView;
