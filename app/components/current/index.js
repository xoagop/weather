import React from 'react';
import {
  View,
  StyleSheet,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Forecast from '../../models/forecast';
import colors from '../../constants/colors';
import Measurement from './measurement';
import resolveIconFor from '../../utils/resolve-icon-for';

const styles = StyleSheet.create({
  container: {
    paddingBottom: 30,
  },
  measurements: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  icon: {
    alignSelf: 'center',
  },
});

const ICON_SIZE = 100;

const CurrentView = ({ forecast }) => (
  <View style={styles.container}>
    <Icon
      style={styles.icon}
      name={resolveIconFor(forecast.icon)}
      size={ICON_SIZE}
      color={colors.accent}
    />
    <View style={styles.measurements}>
      <Measurement
        measurement={`${forecast.precipProbability}%`}
        icon="ios-umbrella"
      />
      <Measurement
        measurement={`${forecast.temperature}\u{00B0}`}
        icon="ios-thermometer"
      />
    </View>
  </View>
);

CurrentView.propTypes = {
  forecast: React.PropTypes.instanceOf(Forecast).isRequired,
};

export default CurrentView;
