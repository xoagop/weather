import React from 'react';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import colors from '../../constants/colors';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  measurement: {
    color: colors.accent,
    paddingLeft: 5,
    fontSize: 20,
  },
});

const ICON_SIZE = 25;

const Measurement = ({ measurement, icon }) => (
  <View style={styles.container}>
    <Icon
      name={icon}
      size={ICON_SIZE}
      color={colors.accent}
    />
    <Text style={styles.measurement}>{measurement}</Text>
  </View>
);

Measurement.propTypes = {
  measurement: React.PropTypes.string.isRequired,
  icon: React.PropTypes.string.isRequired,
};

export default Measurement;
