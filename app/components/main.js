import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Alert,
} from 'react-native';
import connectToStores from 'alt-utils/lib/connectToStores';
import ForecastView from './forecast';
import CurrentView from './current';
import Button from './refresh/button';
import LoadView from './refresh/load-view';
import colors from '../constants/colors';
import ForecastStore from '../stores/forecast-store';
import Weather from '../models/weather';
import {
  ForecastActions,
} from '../actions';

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.main,
    flex: 1,
    paddingTop: 20,
  },
});

const LocationErrorCode = {
  PERMISSION_DENIED: 1,
  POSITION_UNAVAILABLE: 2,
  TIMEOUT: 3,
};

const defaultLocation = {
  latitude: 37.33259551999998,
  longitude: -122.03031802,
};

class Main extends Component {
  static propTypes = {
    weather: React.PropTypes.instanceOf(Weather),
    isRefreshing: React.PropTypes.bool,
  };

  static getStores() {
    return [ForecastStore];
  }

  static getPropsFromStores() {
    return ForecastStore.getState();
  }

  /* eslint-disable no-undef */
  componentDidMount() {
    navigator.geolocation.getCurrentPosition(
      position => {
        const { coords: { latitude, longitude } } = position;
        this.setState({
          latitude,
          longitude,
        });
        ForecastActions.refresh({ latitude, longitude });
      },
      error => {
        if (error.code === LocationErrorCode.PERMISSION_DENIED) {
          Alert.alert('Weather needs access to your location in order to show a local forecast. ' +
            'For now, enjoy the weather in San Francisco.');
        } else {
          Alert.alert('Location unavailable. For now, enjoy the weather in San Francisco.')
        }
        ForecastActions.refresh(defaultLocation);
      },
      {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 1000,
      }
    );
  }
  /* eslint-enable no-unfef */

  refreshWeather = () => {
    ForecastActions.refresh({
      latitude: this.state.latitude || defaultLocation.latitude,
      longitude: this.state.longitude || defaultLocation.longitude,
    });
  };

  render() {
    if (this.props.isRefreshing) {
      return <LoadView />;
    }

    return (
      <View style={styles.container}>
        <CurrentView forecast={this.props.weather.current} />
        <ForecastView dailyForecast={this.props.weather.dailies} />
        <Button onRefresh={this.refreshWeather} />
      </View>
    );
  }
}

export default connectToStores(Main);
