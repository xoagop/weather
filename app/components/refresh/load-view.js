import React from 'react';
import {
  View,
  StyleSheet,
} from 'react-native';
import Spinner from 'react-native-spinkit';
import colors from '../../constants/colors';

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.main,
    flex: 1,
    paddingTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const SPINNER_SIZE = 150;

const LoadView = () => (
  <View style={styles.container}>
    <Spinner
      isVisible
      size={SPINNER_SIZE}
      type="Bounce"
      color={colors.accent}
    />
  </View>
);

export default LoadView;
