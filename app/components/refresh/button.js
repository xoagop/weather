import React from 'react';
import {
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import colors from '../../constants/colors';

const styles = StyleSheet.create({
  container: {
    alignSelf: 'center',
    padding: 20,
  },
});

const ICON_SIZE = 50;

const RefreshButton = ({ onRefresh }) => (
  <TouchableOpacity
    style={styles.container}
    onPress={onRefresh}
  >
    <Icon
      name="ios-refresh-outline"
      size={ICON_SIZE}
      color={colors.accent}
    />
  </TouchableOpacity>
);

RefreshButton.propTypes = {
  onRefresh: React.PropTypes.func.isRequired,
};

export default RefreshButton;
