import {
  AppRegistry,
} from 'react-native';
import Main from './app/components/main';

AppRegistry.registerComponent('weather', () => Main);
