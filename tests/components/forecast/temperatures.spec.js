import React from 'react';
import {
  Text,
} from 'react-native';
import chai, { expect } from 'chai';
import dirtyChai from 'dirty-chai';
import { shallow } from 'enzyme';
import Temperatures from '../../../app/components/forecast/temperatures';
import IoniconsShim from '../../shims/react-native-vector-icons';

chai.use(dirtyChai);

describe('<Temperatures />', () => {
  let sut;

  beforeEach(() => {
    sut = shallow(<Temperatures minTemperature={-6} maxTemperature={10} />);
  });

  it('shows icons for min and max temperatures', () => {
    const icons = sut.find(IoniconsShim);
    expect(icons).to.have.lengthOf(2);
    expect(icons.at(0).props().name).to.equal('ios-arrow-round-down');
    expect(icons.at(1).props().name).to.equal('ios-arrow-round-up');
  });

  it('show min temperature', () => {
    const minTemperature = sut.find(Text).first();
    expect(minTemperature.props().children).to.equal('-6\u{00B0}');
  });

  it('show max temperature', () => {
    const maxTemperature = sut.find(Text).last();
    expect(maxTemperature.props().children).to.equal('10\u{00B0}');
  });
});
