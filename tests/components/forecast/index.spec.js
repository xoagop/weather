import React from 'react';
import chai, { expect } from 'chai';
import dirtyChai from 'dirty-chai';
import { shallow } from 'enzyme';
import ForecastView from '../../../app/components/forecast';
import Dailies from '../../../app/components/forecast/dailies';

chai.use(dirtyChai);

describe('<ForecastView />', () => {
  it('creates <Dailies /> from given forecast', () => {
    const sut = shallow(<ForecastView dailyForecast={[]} />);
    const dailies = sut.find(Dailies);
    expect(dailies).to.have.lengthOf(1);
    expect(dailies.props().dailyForecast).to.be.empty();
  });
});
