import React from 'react';
import chai, { expect } from 'chai';
import dirtyChai from 'dirty-chai';
import { shallow } from 'enzyme';
import Daily from '../../../app/components/forecast/daily';
import IoniconsShim from '../../shims/react-native-vector-icons';
import Forecast from '../../../app/models/forecast';
import colors from '../../../app/constants/colors';
import Temperatures from '../../../app/components/forecast/temperatures';

chai.use(dirtyChai);

describe('<Daily />', () => {
  let forecast;

  beforeEach(() => {
    forecast = Forecast.fromObject({
      minTemperature: -5,
      maxTemperature: 10,
    });
  });

  const render = () => {
    const sut = shallow(<Daily forecast={forecast} />);
    return sut.find(IoniconsShim);
  };

  it('sets colors.accent as icon color', () => {
    forecast.icon = 'rain';
    const icon = render();
    expect(icon.props().color).to.equal(colors.accent);
  });

  describe('when forecast icon is rain', () => {
    it('shows ios-rainy icon', () => {
      forecast.icon = 'rain';
      const icon = render();
      expect(icon.props().name).to.equal('ios-rainy');
    });
  });

  describe('when forecast icon is clear-day', () => {
    it('shows ios-sunny icon', () => {
      forecast.icon = 'clear-day';
      const icon = render();
      expect(icon.props().name).to.equal('ios-sunny');
    });
  });

  describe('when forecast icon is cloudy', () => {
    it('shows ios-cloudy icon', () => {
      forecast.icon = 'cloudy';
      const icon = render();
      expect(icon.props().name).to.equal('ios-cloudy');
    });
  });

  describe('when forecast icon is partly-cloudy-night', () => {
    it('shows ios-cloudy-night icon', () => {
      forecast.icon = 'partly-cloudy-night';
      const icon = render();
      expect(icon.props().name).to.equal('ios-cloudy-night');
    });
  });

  describe('when forecast icon is snow', () => {
    it('shows ios-snow icon', () => {
      forecast.icon = 'snow';
      const icon = render();
      expect(icon.props().name).to.equal('ios-snow');
    });
  });

  describe('when forecast icon is not supported', () => {
    it('shows ios-party-sunny icon', () => {
      forecast.icon = 'not-supported';
      const icon = render();
      expect(icon.props().name).to.equal('ios-partly-sunny');
    });
  });

  it('passes forecast temperatures to <Temperatures /> ', () => {
    const sut = shallow(<Daily forecast={forecast} />);
    const temperatures = sut.find(Temperatures);
    const { minTemperature, maxTemperature } = temperatures.props();
    expect(minTemperature).to.equal(forecast.minTemperature);
    expect(maxTemperature).to.equal(forecast.maxTemperature);
  });
});
