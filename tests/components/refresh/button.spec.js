import React from 'react';
import chai, { expect } from 'chai';
import dirtyChai from 'dirty-chai';
import { shallow } from 'enzyme';
import sinon from 'sinon';
import Button from '../../../app/components/refresh/button';

chai.use(dirtyChai);

describe('<Button />', () => {
  it('calls onRefresh when pressed', () => {
    const onRefresh = sinon.spy();
    const sut = shallow(<Button onRefresh={onRefresh} />);
    sut.simulate('press');
    expect(onRefresh.calledOnce).to.be.true();
  });
});
