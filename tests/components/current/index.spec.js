import React from 'react';
import {

} from 'react-native';
import chai, { expect } from 'chai';
import dirtyChai from 'dirty-chai';
import { shallow } from 'enzyme';
import CurrentView from '../../../app/components/current';
import Forecast from '../../../app/models/forecast';
import Measurement from '../../../app/components/current/measurement';

chai.use(dirtyChai);

describe('<CurrentView />', () => {
  let forecast;

  beforeEach(() => {
    forecast = Forecast.fromObject({
      icon: 'test-icon',
      temperature: 10,
      minTemperature: 5,
      maxTemperature: 15,
      precipProbability: 20,
      time: 1476342000,
    });
  });

  it('creates measurement for temperature', () => {
    const sut = shallow(<CurrentView forecast={forecast} />);
    const temperature = sut.find(Measurement).last();
    expect(temperature.props().measurement).to.equal('10\u{00B0}');
  });

  it('creates measurement for precipProbability', () => {
    const sut = shallow(<CurrentView forecast={forecast} />);
    const temperature = sut.find(Measurement).first();
    expect(temperature.props().measurement).to.equal('20%');
  });
});
