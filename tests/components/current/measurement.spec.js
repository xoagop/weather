import React from 'react';
import {
  Text,
} from 'react-native';
import chai, { expect } from 'chai';
import dirtyChai from 'dirty-chai';
import { shallow } from 'enzyme';
import Measurement from '../../../app/components/current/measurement';
import IoniconsShim from '../../shims/react-native-vector-icons';

chai.use(dirtyChai);

describe('<Measurement />', () => {
  it('shows given measurement', () => {
    const sut = shallow(<Measurement measurement="10%" icon="test" />);
    const label = sut.find(Text);
    expect(label.props().children).to.equal('10%');
  });

  it('shows given icon', () => {
    const sut = shallow(<Measurement measurement="" icon="test-icon" />);
    const icon = sut.find(IoniconsShim);
    expect(icon.props().name).to.equal('test-icon');
  });
});
