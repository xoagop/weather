import chai, { expect } from 'chai';
import dirtyChai from 'dirty-chai';
import alt from '../../app/alt';
import ForecastStore from '../../app/stores/forecast-store';
import Weather from '../../app/models/weather';
import {
  ForecastActions,
} from '../../app/actions';

chai.use(dirtyChai);

describe('ForecastStore', () => {
  describe('when ForecastActions.refreshSucceeded occurs', () => {
    it('updates state with given weather', () => {
      const weather = Weather.fromObject({ current: {} });
      ForecastActions.refreshSucceeded(weather);
      expect(ForecastStore.getState().weather).to.eql(weather);
    });
  });

  describe('when ForecastActions.refreshFailed occurs', () => {
    it('clears the state with empty weather', () => {
      ForecastActions.refreshFailed();
      expect(ForecastStore.getState().weather).to.eql(Weather.empty());
    });
  });

  afterEach(() => {
    alt.recycle(ForecastStore);
  });
});
