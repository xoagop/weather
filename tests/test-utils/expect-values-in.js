import R from 'ramda';
import { expect } from 'chai';

const expectValuesIn = (itemToTest) => ({
  toBeEqualWith: (expected) => {
    const expectationKeys = R.keys(expected);
    R.forEach(key => {
      expect(itemToTest[key]).to.equal(expected[key]);
    }, expectationKeys);
  },
});

export default expectValuesIn;
