import React from 'react';
import createShim from './create-shim';

const RN = React;

const createListViewShim = () => {
  const DataSourceBase = createShim('DataSource');
  class DataSource extends DataSourceBase {
    cloneWithRows = (items) => items;
  }
  const ListView = createShim('ListView');
  ListView.DataSource = DataSource;
  return ListView;
};

RN.View = createShim('View');
RN.Text = createShim('Text');
RN.TouchableHighlight = createShim('TouchableHighlight');
RN.TextInput = createShim('TextInput');
RN.ListView = createListViewShim();
RN.RefreshControl = createShim('RefreshControl');
RN.TouchableOpacity = createShim('TouchableOpacity');
RN.Image = createShim('Image');

export default RN;
