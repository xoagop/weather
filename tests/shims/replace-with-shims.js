import ReactNativeShim from './react-native';
import IoniconsShim from './react-native-vector-icons';

const reactNativeKey = require.resolve('react-native');
const ioniconsKey = require.resolve('react-native-vector-icons/Ionicons');

require.cache[reactNativeKey] = {
  id: reactNativeKey,
  filename: reactNativeKey,
  loaded: true,
  exports: ReactNativeShim,
};

require.cache[ioniconsKey] = {
  id: ioniconsKey,
  filename: ioniconsKey,
  loaded: true,
  exports: IoniconsShim,
};
