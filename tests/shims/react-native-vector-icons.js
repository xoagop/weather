import createShim from './create-shim';

const IoniconsShim = createShim('Ionicons');

export default IoniconsShim;
