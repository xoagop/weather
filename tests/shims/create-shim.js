import React, { Component } from 'react';

const RN = React;

export const PropTypes = React.PropTypes;

RN.StyleSheet = {
  create: style => style,
};

const createShim = type => class extends Component {
  static propTypes = {
    children: React.PropTypes.node,
  };

  constructor(props) {
    super(props);
    this.displayName = type;
  }

  render() {
    return (
      <div {...this.props}>
        {this.props.children}
      </div>
    );
  }
};

export default createShim;
