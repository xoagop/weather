import chai, { expect } from 'chai';
import dirtyChai from 'dirty-chai';
import groupBy from '../../app/utils/group-by';

chai.use(dirtyChai);

describe('groupBy', () => {
  let items;

  beforeEach(() => {
    items = [{
      name: 'banana',
      category: 'fruit',
    }, {
      name: 'apple',
      category: 'fruit',
    }, {
      name: 'cucumber',
      category: 'vegetable',
    }];
  });

  it('returns an empty object when given an empty list', () => {
    expect(groupBy('test', [])).to.be.empty();
  });

  it('returns an empty object when given a non-existing key', () => {
    expect(groupBy('test', items)).to.be.empty();
  });

  it('returns an object with items grouped by key', () => {
    const resultItem = groupBy('category', items);
    expect(resultItem).to.have.all.keys(['fruit', 'vegetable']);
    expect(resultItem.fruit).to.have.lengthOf(2);
    expect(resultItem.vegetable).to.have.lengthOf(1);
  });

  it('transforms items in groups based on given transformation', () => {
    const transform = (item) => {
      const toUpdate = item;
      toUpdate.name = toUpdate.name.toUpperCase();
      return toUpdate;
    };
    const resultItem = groupBy('category', items, transform);
    expect(resultItem).to.have.all.keys(['fruit', 'vegetable']);
    expect(resultItem.fruit[0].name).to.equal('BANANA');
    expect(resultItem.fruit[1].name).to.equal('APPLE');
    expect(resultItem.vegetable[0].name).to.equal('CUCUMBER');
  });
});
