import chai, { expect } from 'chai';
import dirtyChai from 'dirty-chai';
import resolveIconFor, { forecastIcons } from '../../app/utils/resolve-icon-for';

chai.use(dirtyChai);

describe('resolveIconFor', () => {
  it('returns ios-rainy for forecastIcons.rain', () => {
    expect(resolveIconFor(forecastIcons.rain)).to.equal('ios-rainy');
  });

  it('returns ios-sunny for forecastIcons.clear', () => {
    expect(resolveIconFor(forecastIcons.clear)).to.equal('ios-sunny');
  });

  it('returns ios-cloudy for forecastIcons.cloudy', () => {
    expect(resolveIconFor(forecastIcons.cloudy)).to.equal('ios-cloudy');
  });

  it('returns ios-cloudy-night for forecastIcons.partlyCloudyNight', () => {
    expect(resolveIconFor(forecastIcons.partlyCloudyNight)).to.equal('ios-cloudy-night');
  });

  it('returns ios-snow for forecastIcons.snow', () => {
    expect(resolveIconFor(forecastIcons.snow)).to.equal('ios-snow');
  });

  it('returns ios-partly-sunny as default', () => {
    expect(resolveIconFor('unsupported')).to.equal('ios-partly-sunny');
  });
});
