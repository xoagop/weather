import chai, { expect } from 'chai';
import dirtyChai from 'dirty-chai';
import R from 'ramda';
import Weather from '../../app/models/weather';
import expectValuesIn from '../test-utils/expect-values-in';

chai.use(dirtyChai);

describe('Weather', () => {
  describe('fromApiResponse', () => {
    let response;

    beforeEach(() => {
      const weekday = {
        icon: 'rain',
        precipProbability: 20,
        temperatureMax: 20,
        temperatureMin: 10,
        time: 1476342000,
      };

      response = {
        currently: {
          icon: 'rain',
          temperature: 10,
          precipProbability: 20,
          time: 1476342000,
        },
        daily: {
          data: R.repeat(weekday, 8),
        },
      };
    });

    it('returns null when response is null', () => {
      expect(Weather.fromApiResponse(null)).to.be.null();
    });

    it('returns null when response is not given', () => {
      expect(Weather.fromApiResponse()).to.be.null();
    });

    it('fills current weather', () => {
      const weather = Weather.fromApiResponse(response);
      const [current] = response.daily.data;

      expectValuesIn(weather.current).toBeEqualWith({
        icon: response.currently.icon,
        temperature: response.currently.temperature,
        precipProbability: response.currently.precipProbability,
        minTemperature: current.temperatureMin,
        maxTemperature: current.temperatureMax,
      });
    });

    it('fills weekdays', () => {
      const weather = Weather.fromApiResponse(response);
      const [weekday] = response.daily.data;

      R.forEach(w => {
        expectValuesIn(w).toBeEqualWith({
          icon: weekday.icon,
          precipProbability: weekday.precipProbability,
          maxTemperature: weekday.temperatureMax,
          minTemperature: weekday.temperatureMin,
        });
      }, weather.dailies);
    });
  });
});
