import chai, { expect } from 'chai';
import dirtyChai from 'dirty-chai';
import Forecast from '../../app/models/forecast';
import expectValuesIn from '../test-utils/expect-values-in';

chai.use(dirtyChai);

describe('Forecast', () => {
  describe('constructor', () => {
    it('creates a moment out of given unix time', () => {
      const forecast = new Forecast('test', 20, 20, 25, 30, 1476342000);
      expect(forecast.time.format('ddd')).to.equal('Thu');
    });

    it('creates forecast', () => {
      const expected = {
        icon: 'test',
        minTemperature: 5,
        maxTemperature: 10,
      };

      const forecast = Forecast.fromObject(expected);
      expectValuesIn(forecast).toBeEqualWith(expected);
    });
  });

  describe('fromApiResponse', () => {
    it('creates a forecast', () => {
      const response = {
        icon: 'rain',
        precipProbability: 20,
        temperatureMax: 20,
        temperatureMin: 10,
        time: 1476342000,
      };
      const forecast = Forecast.fromApiResponse(response);
      expectValuesIn(forecast).toBeEqualWith({
        icon: response.icon,
        precipProbability: response.precipProbability,
        minTemperature: response.temperatureMin,
        maxTemperature: response.temperatureMax,
      });
    })
  });
});
